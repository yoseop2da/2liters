//
//  HistoryViewController.m
//  2Liters
//
//  Created by 박요섭 on 2015. 2. 11..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "HistoryViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface HistoryViewController ()

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"History";
    
    NSMutableDictionary *historyDictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"historyDictionary"] mutableCopy];
    if (!historyDictionary) {
        historyDictionary = [NSMutableDictionary dictionary];
    }
    
    [DrinkManager sharedInstance].historyDictionary = historyDictionary;
    
    self.bannerView.adUnitID = @"ca-app-pub-2783299302427084/9869202851";
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[GADRequest request]];
}

- (IBAction)closeButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)resetButtonTouched:(id)sender
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"historyDictionary"];
    [DrinkManager sharedInstance].historyDictionary = nil;
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [DrinkManager sharedInstance].historyDictionary.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    NSDictionary *dic = [DrinkManager sharedInstance].historyDictionary;
    NSArray *allKyes = [self sortedAllKeys];
    
    cell.textLabel.text = allKyes[indexPath.row];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",dic[cell.textLabel.text]];
    return cell;
}

- (NSArray *)sortedAllKeys
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterMediumStyle;
    NSArray *allKeys = [DrinkManager sharedInstance].historyDictionary.allKeys;
    
//    NSMutableArray *dateArray = [NSMutableArray array];
//    for (NSString *dateString in allKeys) {
//        NSDate *date = [formatter dateFromString:dateString];
//        [dateArray addObject:date];
//    }
    
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending: NO];
    NSArray *keyArray = [allKeys sortedArrayUsingDescriptors:@[sortOrder]];

//    NSMutableArray *sortedArray = [NSMutableArray array];
//    for (NSDate *date in keyArray) {
//        NSString *dateString = [formatter stringFromDate:date];
//        [sortedArray addObject:dateString];
//    }
    
    return keyArray;
}

@end
