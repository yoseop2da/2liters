//
//  HistoryViewController.h
//  2Liters
//
//  Created by 박요섭 on 2015. 2. 11..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrinkManager.h"
@import GoogleMobileAds;

@interface HistoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

@end
