//
//  DrinkManager.m
//  2Liters
//
//  Created by Bora on 2015. 2. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "DrinkManager.h"
#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#define SharedAppDelegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])
@interface DrinkManager()

@end

@implementation DrinkManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    static DrinkManager *_sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [DrinkManager new];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{   
    NSInteger cupSize = [[NSUserDefaults standardUserDefaults] integerForKey:@"cupSize"];
    if (cupSize) {
        self.cupSize = cupSize;
    }else{
        self.cupSize = 300;
        [[NSUserDefaults standardUserDefaults] setInteger:self.cupSize forKey:@"cupSize"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    self.isAllowNoti = NO;
    
    NSArray *notiArray = [[NSUserDefaults standardUserDefaults] valueForKey:@"notiArray"];
    if (notiArray) {
        self.dataArray = notiArray;
    }else{
        notiArray = @[
                      @{@"time": @"6",@"isAm":@(1),@"isOn":@(0)},
                      @{@"time": @"7",@"isAm":@(1),@"isOn":@(0)},
                      @{@"time": @"8",@"isAm":@(1),@"isOn":@(0)},
                      @{@"time": @"9",@"isAm":@(1),@"isOn":@(1)},
                      @{@"time": @"10",@"isAm":@(1),@"isOn":@(0)},
                      @{@"time": @"11",@"isAm":@(1),@"isOn":@(1)},
                      @{@"time": @"12",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"1",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"2",@"isAm":@(0),@"isOn":@(1)},
                      @{@"time": @"3",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"4",@"isAm":@(0),@"isOn":@(1)},
                      @{@"time": @"5",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"6",@"isAm":@(0),@"isOn":@(1)},
                      @{@"time": @"7",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"8",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"9",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"10",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"11",@"isAm":@(0),@"isOn":@(0)},
                      @{@"time": @"12",@"isAm":@(1),@"isOn":@(0)}
                      ];
        self.dataArray = notiArray;
        [[NSUserDefaults standardUserDefaults] setValue:notiArray forKey:@"notiArray"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    for (NSDictionary *dic in self.dataArray) {
        if ([dic[@"isOn"] boolValue]) {
            self.isAllowNoti = YES;
            NSInteger time = [dic[@"time"] integerValue];
            if (![dic[@"isAm"] boolValue]) {
                time += 12;
            }
            [SharedAppDelegate addAlarmWithHour:time];
        }
    }
}

- (void)setCupSize:(NSInteger)cupSize
{
    _cupSize = cupSize;
    [[NSUserDefaults standardUserDefaults] setInteger:_cupSize forKey:@"cupSize"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _increaseValue = ceil((_cupSize * [UIScreen mainScreen].bounds.size.height) / 2000);
}

- (void)changeAllNotiWithStatus:(BOOL)isOn
{
    NSMutableArray *mutableArray = [NSMutableArray new];
    for (NSDictionary *notiDic in self.dataArray) {
        NSMutableDictionary *newDic = [notiDic mutableCopy];
        if (isOn) {
            [self addNoti:notiDic];
        }else{
            [self removeNoti:notiDic];
        }
        [newDic setObject:[NSNumber numberWithBool:isOn] forKey:@"isOn"];
        [mutableArray addObject:newDic];
    }
    [[NSUserDefaults standardUserDefaults] setValue:mutableArray forKey:@"notiArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.dataArray = mutableArray;
    self.isAllowNoti = isOn;
}

- (void)changeNoti:(NSInteger)index isOn:(BOOL)isOn
{
    NSMutableDictionary *notiDic = [self.dataArray[index] mutableCopy];
    [notiDic setObject:[NSNumber numberWithBool:isOn] forKey:@"isOn"];
    NSMutableArray *mutableArray = [self.dataArray mutableCopy];
    [mutableArray replaceObjectAtIndex:index withObject:notiDic];
    [[NSUserDefaults standardUserDefaults] setValue:mutableArray forKey:@"notiArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.dataArray = mutableArray;
    
    self.isAllowNoti = NO;
    for (NSDictionary *dic in self.dataArray) {
        if ([dic[@"isOn"] boolValue]) {
            self.isAllowNoti = YES;
        }
    }
    
    if (isOn) {
        [self addNoti:notiDic];
    }else{
        [self removeNoti:notiDic];
    }
}

- (void)addNoti:(NSDictionary *)notiDic
{
    NSInteger time = [notiDic[@"time"] integerValue];
    if (![notiDic[@"isAm"] boolValue]) {
        time += 12;
    }
    [SharedAppDelegate addAlarmWithHour:time];
}

- (void)removeNoti:(NSDictionary *)notiDic
{
    NSInteger time = [notiDic[@"time"] integerValue];
    if (![notiDic[@"isAm"] boolValue]) {
        time += 12;
    }
    [SharedAppDelegate cancelNotificationWithItem:time];
}

-(NSDictionary *)historyDictionary
{
    NSArray *allKeys = _historyDictionary.allKeys;
    for (NSString *key in allKeys) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterMediumStyle;

        NSDate *date = [formatter dateFromString:key];
        if (!date) {
            formatter.dateFormat = @"yyyy. MM. dd.";
            date = [formatter dateFromString:key];
            if (!date) {
                formatter.dateFormat = @"MMM dd, yyyy";
                date = [formatter dateFromString:key];
            }
            formatter.dateFormat = @"MMM dd, yyyy";
            NSString *string = [formatter stringFromDate:date];
            NSMutableDictionary *dic = [_historyDictionary mutableCopy];
            if (string && ![string isEqualToString:key]) {
                dic[string] = dic[key];
                [dic removeObjectForKey:key];
            }
            _historyDictionary = dic;
            [[NSUserDefaults standardUserDefaults] setObject:_historyDictionary forKey:@"historyDictionary"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    return _historyDictionary;
}

@end
