//
//  DrinkManager.h
//  2Liters
//
//  Created by Bora on 2015. 2. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DrinkManager : NSObject

@property (assign, nonatomic) NSInteger cupSize;
@property (assign) NSInteger increaseValue;
@property (strong,nonatomic) NSDictionary *historyDictionary;
@property (strong) NSArray *dataArray;
@property (assign) BOOL isAllowNoti;

+ (instancetype)sharedInstance;
- (void)initialize;
- (void)changeAllNotiWithStatus:(BOOL)isOn;
- (void)changeNoti:(NSInteger)index isOn:(BOOL)isOn;
@end
