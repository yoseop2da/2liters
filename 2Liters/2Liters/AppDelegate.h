//
//  AppDelegate.h
//  2Liters
//
//  Created by 박요섭 on 2015. 2. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)addAlarmWithHour:(NSInteger)hour;
- (void)cancelNotificationWithItem:(NSInteger)time;
@end

