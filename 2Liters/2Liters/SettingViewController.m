//
//  SettingViewController.m
//  2Liters
//
//  Created by 박요섭 on 2015. 3. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "SettingViewController.h"
#import "SettingCell.h"
#import "DrinkManager.h"

@interface SettingViewController ()<SettingCellDelegate>

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Notifications";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 0) {
        return 1;
    }else{
        return [DrinkManager sharedInstance].dataArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell"];
    cell.indexPath = indexPath;
    cell.delegate = self;
    if(indexPath.section == 0){
        cell.timeLabel.text = @"Allow All Notifications";
        cell.ampmLabel.text = @"";
        cell.switchButton.on = [DrinkManager sharedInstance].isAllowNoti;
        
    }else{
        NSDictionary *dic =  [DrinkManager sharedInstance].dataArray[indexPath.row];
        NSString *timeString = [dic[@"time"] stringByAppendingString:@":00"];
        cell.timeLabel.text = timeString;
        cell.ampmLabel.text = [dic[@"isAm"] boolValue] ? @"AM": @"PM";
        cell.switchButton.on = [dic[@"isOn"] boolValue];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)switchButtonTouched:(NSIndexPath *)indexPath status:(BOOL)isOn
{
    if (indexPath.section == 0) {
        [[DrinkManager sharedInstance] changeAllNotiWithStatus:isOn];
    }else{
        [[DrinkManager sharedInstance] changeNoti:indexPath.row isOn:isOn];
    }
    
    [self.tableView reloadData];

}

- (IBAction)closeButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
