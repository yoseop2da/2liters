//
//  SettingCell.m
//  2Liters
//
//  Created by 박요섭 on 2015. 3. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "SettingCell.h"

@implementation SettingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchButtonTouched:(id)sender
{
    [self.delegate switchButtonTouched:self.indexPath status:self.switchButton.on];
}

@end
