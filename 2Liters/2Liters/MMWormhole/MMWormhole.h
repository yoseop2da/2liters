//
// MMWormhole.h
//
#import <Foundation/Foundation.h>

@protocol MMWormholeTransiting;

NS_ASSUME_NONNULL_BEGIN

//! Project version number for MMWormhole.
FOUNDATION_EXPORT double MMWormholeVersionNumber;

//! Project version string for MMWormhole.
FOUNDATION_EXPORT const unsigned char MMWormholeVersionString[];

@interface MMWormhole : NSObject

@property (nonatomic, strong) id<MMWormholeTransiting> wormholeMessenger;

- (instancetype)initWithApplicationGroupIdentifier:(NSString *)identifier
                                 optionalDirectory:(nullable NSString *)directory NS_DESIGNATED_INITIALIZER;

- (void)passMessageObject:(nullable id <NSCoding>)messageObject
			   identifier:(nullable NSString *)identifier;

- (nullable id)messageWithIdentifier:(nullable NSString *)identifier;
- (void)clearMessageContentsForIdentifier:(nullable NSString *)identifier;
- (void)clearAllMessageContents;

/**
 이벤트를 케치.
 */
- (void)listenForMessageWithIdentifier:(nullable NSString *)identifier
                              listener:(nullable void (^)(__nullable id messageObject))listener;

- (void)stopListeningForMessageWithIdentifier:(nullable NSString *)identifier;

@end


@protocol MMWormholeTransiting <NSObject>
- (BOOL)writeMessageObject:(nullable id<NSCoding>)messageObject forIdentifier:(NSString *)identifier;
- (nullable id<NSCoding>)messageObjectForIdentifier:(nullable NSString *)identifier;
- (void)deleteContentForIdentifier:(nullable NSString *)identifier;
- (void)deleteContentForAllMessages;

@end

NS_ASSUME_NONNULL_END
