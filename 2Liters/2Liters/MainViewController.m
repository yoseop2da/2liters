//
//  ViewController.m
//  2Liters
//
//  Created by 박요섭 on 2015. 2. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "MainViewController.h"
#import "DrinkManager.h"
#import "SettingViewController.h"
// 사운드
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/CALayer.h>

#import "MMWormhole.h"

@interface MainViewController ()<UIAlertViewDelegate>
{
    int _percentage;
    int _waterValue;
    BOOL _isTouched;
}

@property (nonatomic, strong) MMWormhole *wormhole;

@property (strong) AVAudioPlayer *audioPlayer;


@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cupButtons;
@property (weak, nonatomic) IBOutlet UIView *cupView;
@property (weak, nonatomic) IBOutlet UIButton *dimButton;
@property (weak, nonatomic) IBOutlet UIView *waterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *waterViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *guideLabel;

@property (weak, nonatomic) IBOutlet UIButton *cupSelectButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cupViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cupViewBottom;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refresh];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"RefreshMainView" object:nil];
    
}

- (void)refresh
{
    [self setWaterFromHistory];
    
    self.cupView.layer.masksToBounds = YES;
    
    for (UIButton *button in self.cupButtons) {
        if ([DrinkManager sharedInstance].cupSize == button.tag)
            button.selected = YES;
    }
    
    if (_percentage == 0) {
        [self showCupView];
    }else{
        [self hideCupView];
    }
    
    self.guideLabel.hidden = YES;
    
    [self addWatchObserver];
}

- (void)addWatchObserver
{
    self.wormhole = [[MMWormhole alloc] initWithApplicationGroupIdentifier:@"group.yoseop2da.2liters"
                                                         optionalDirectory:@"2Liters"];
}

- (void)post2LitersData
{
    [self.wormhole passMessageObject:@{@"percentageString":self.percentageLabel.text, @"mlString":self.mlLabel.text} identifier:@"DataFromiPhone"];
}

- (void)setWaterFromHistory
{
    NSMutableDictionary *historyDictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"historyDictionary"] mutableCopy];
    if (historyDictionary) {
        NSString *valueString = historyDictionary[[self todayString]];
        if (valueString) {
            valueString = [valueString stringByReplacingOccurrencesOfString:@"ml" withString:@""];
            valueString = [valueString stringByReplacingOccurrencesOfString:@"% /" withString:@"=="];
            NSArray *arr = [valueString componentsSeparatedByString:@"=="];
            _percentage = [arr.firstObject intValue];
            _waterValue = [arr.lastObject intValue];
        }else{
            _percentage = 0;
            _waterValue = 0;
        }
        
        self.waterViewHeightConstraint.constant = ceil((_waterValue * [UIScreen mainScreen].bounds.size.height) / 2000);
        self.percentageLabel.text = [NSString stringWithFormat:@"%d%%", _percentage];
        self.mlLabel.text = [NSString stringWithFormat:@"%dml", _waterValue];
    }

}

- (IBAction)openCupViewButtonTouched:(id)sender
{
    [self showCupView];
}

- (IBAction)drinkButtonTouched:(id)sender {
    
    [self drinkActionWithCompletion:nil];
}

- (void)drinkActionWithCompletion:(void (^)(NSDictionary *dic))completion
{
    if (_percentage >= 100)
    {
        [self showCompleteAlertView];
        return;
    }
    
    if (_isTouched)
        return;
    _isTouched = YES;
    
    [self playSound];
    
    [UIView animateWithDuration:1.0f animations:^{
        self.waterViewHeightConstraint.constant += [DrinkManager sharedInstance].increaseValue;
        [self.waterView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        _isTouched = NO;
        _percentage = (self.waterViewHeightConstraint.constant * 100) / [UIScreen mainScreen].bounds.size.height;
        _waterValue += [DrinkManager sharedInstance].cupSize;
        
        [self setButtonTypeWithWhite:YES];
        
        if (_percentage >= 100)
        {
            _percentage = 100;
            _waterValue = 2000;
        }
        
        self.percentageLabel.text = [NSString stringWithFormat:@"%d%%", _percentage];
        self.mlLabel.text = [NSString stringWithFormat:@"%dml", _waterValue];
        
        [self saveHistory];
        
        [self post2LitersData];
        if (completion) {
            completion(@{@"Percentage": self.percentageLabel.text,
                         @"Water": self.mlLabel.text});
        }
    }];
}

- (IBAction)settingButtonTouched:(id)sender
{

}

- (UIImage *)capturedImage:(UIView *)kView
{
    UIGraphicsBeginImageContext(kView.bounds.size);
    [kView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *myImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return myImage;
}

- (IBAction)shareButtonTouched:(id)sender
{
    // TODO : 공유하기.
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"YYYY/MM/dd";
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSString *shareString = [NSString stringWithFormat:@"I drank %dml waters on %@\n", _waterValue,dateString];
    UIImage *screenShotImage = [self capturedImage:self.view];
    NSString *shareUrlString = @"https://appsto.re/kr/ezXT5.i";
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[shareString, shareUrlString,screenShotImage] applicationActivities:@[]];
    
    [self presentViewController:activityViewController
                                       animated:YES
                                     completion:nil];
}

- (IBAction)cupButtonTouched:(UIButton *)cupButton
{
    // 컵 선택하기.
    [DrinkManager sharedInstance].cupSize = cupButton.tag;
    for (UIButton *button in self.cupButtons) {
        button.selected = (cupButton.tag == button.tag);
    }
    
    [self.dimButton setTitle:@"Tap here, Let's drink water" forState:UIControlStateNormal];
}

- (IBAction)dimButtonButtonTouched:(id)sender
{
    [self hideCupView];
}

- (void)showCupView
{
    self.dimButton.hidden = NO;
    self.guideLabel.hidden = YES;
    
    [UIView animateWithDuration:0.5f animations:^{
        self.cupViewBottom.constant = 0;
        [self.cupView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.dimButton setTitle:@"Select your cup" forState:UIControlStateNormal];
    }];
}

- (void)hideCupView
{
    self.dimButton.hidden = YES;
    self.guideLabel.hidden = NO;
    [UIView animateWithDuration:0.5f animations:^{
        [self.dimButton setTitle:@"" forState:UIControlStateNormal];
        self.cupViewBottom.constant = -180;
        [self.cupView layoutIfNeeded];
    }];
}

#pragma mark - History
- (void)saveHistory
{
    NSString *dateKey = [self todayString];

    NSMutableDictionary *historyDictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"historyDictionary"] mutableCopy];
    if (!historyDictionary) { historyDictionary = [NSMutableDictionary dictionary];}
    
    [historyDictionary setValue:[NSString stringWithFormat:@"%d%% / %dml",_percentage,_waterValue] forKey:dateKey];
    [DrinkManager sharedInstance].historyDictionary = historyDictionary;
    
    [[NSUserDefaults standardUserDefaults] setObject:[DrinkManager sharedInstance].historyDictionary forKey:@"historyDictionary"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)todayString
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"MMM dd, yyyy";
//    formatter.dateStyle = NSDateFormatterMediumStyle;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"/*@"ko_KR"*/];
    formatter.locale = locale;
    NSString *dateKey = [formatter stringFromDate:[NSDate date]];
    return dateKey;
}

#pragma mark - Alert Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        _percentage = 0;
        _waterValue = 0;
        self.percentageLabel.text = [NSString stringWithFormat:@"%d%%", _percentage];
        self.mlLabel.text = [NSString stringWithFormat:@"%dml", _waterValue];
        
        [UIView animateWithDuration:1.5f animations:^{
            self.waterViewHeightConstraint.constant = 0;
            [self.waterView layoutIfNeeded];
        }completion:^(BOOL finished) {
            [self setButtonTypeWithWhite:NO];
        }];
        
        [self post2LitersData];
    }
}

- (void)showCompleteAlertView
{
    [[[UIAlertView alloc] initWithTitle:@"Complete" message:@"Try again?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil, nil] show];
}

- (void)playSound
{
    NSString *pathToFile = [[NSBundle mainBundle] pathForResource:@"water" ofType:@"m4a"];
    NSURL *url =[NSURL fileURLWithPath:pathToFile];
    
    AVAudioPlayer *newAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
    self.audioPlayer = newAudioPlayer;
    self.audioPlayer.numberOfLoops = 0;
    [self.audioPlayer play];
}

- (void)setButtonTypeWithWhite:(BOOL)isWhite
{
    self.cupSelectButton.selected = isWhite;
    self.shareButton.selected = isWhite;
    self.historyButton.selected = isWhite;
}

@end
