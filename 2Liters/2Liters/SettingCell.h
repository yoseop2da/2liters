//
//  SettingCell.h
//  2Liters
//
//  Created by 박요섭 on 2015. 3. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SettingCellDelegate

- (void)switchButtonTouched:(NSIndexPath *)indexPath status:(BOOL )isOn;

@end

@interface SettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *switchButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ampmLabel;

@property (strong) NSIndexPath *indexPath;
@property (strong) id<SettingCellDelegate> delegate;
@end
