//
//  ViewController.h
//  2Liters
//
//  Created by 박요섭 on 2015. 2. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet UILabel *mlLabel;

- (void)drinkActionWithCompletion:(void (^)(NSDictionary *dic))completion;

@end

