//
//  main.m
//  2Liters
//
//  Created by 박요섭 on 2015. 2. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
