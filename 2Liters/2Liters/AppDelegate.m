//
//  AppDelegate.m
//  2Liters
//
//  Created by 박요섭 on 2015. 2. 8..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "AppDelegate.h"
#import "DrinkManager.h"
#import "MainViewController.h"
@interface AppDelegate ()

@property (strong) MainViewController *mainViewController;
@property (strong) UIStoryboard *mainStoryboard;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    for (UILocalNotification *noti in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        [[UIApplication sharedApplication] cancelLocalNotification:noti];
    }
    
    if (!launchOptions[UIApplicationLaunchOptionsLocalNotificationKey])
    {
        UIUserNotificationType types = UIUserNotificationTypeBadge| UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSetting = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSetting];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.mainViewController = [self.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([MainViewController class])];
    self.window.rootViewController = self.mainViewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {}

- (void)applicationDidEnterBackground:(UIApplication *)application {}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[DrinkManager sharedInstance] initialize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshMainView" object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {}

- (void)applicationWillTerminate:(UIApplication *)application {}


- (void)addAlarmWithHour:(NSInteger)hour
{
    [self cancelNotificationWithItem:hour];

    UILocalNotification *localNotification = [UILocalNotification new];
    localNotification.timeZone = [NSTimeZone systemTimeZone];
    NSUInteger randomNumber = arc4random_uniform(2);
    NSArray *bodyStrings = @[@"Hey, Let's drink 2liters water",@"Hey, It's time to drink water"];
    localNotification.alertBody = bodyStrings[randomNumber%2];
    localNotification.repeatInterval = kCFCalendarUnitDay;
    localNotification.fireDate = [self makeDateWithHour:hour];
    localNotification.userInfo = @{@"time":[NSString stringWithFormat:@"%ld",hour]};
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    NSLog(@"fireDate : %@", localNotification);
}

- (NSDate *)makeDateWithHour:(NSInteger)hour
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *todayDateComponents = [calendar components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond) fromDate:today];
    todayDateComponents.hour = hour;
    todayDateComponents.minute = 0;
    todayDateComponents.second = 0;
    
    NSDate *date = [calendar dateFromComponents:todayDateComponents];
    return date;
}

- (void)cancelNotificationWithItem:(NSInteger)time
{
    for (UILocalNotification *noti in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if ([noti.userInfo[@"time"] integerValue] == time)
            [[UIApplication sharedApplication] cancelLocalNotification:noti];
    }
}

#pragma mark - Apple Watch

- (void)application:(UIApplication *)application handleWatchKitExtensionRequest:(NSDictionary *)userInfo reply:(void(^)(NSDictionary *replyInfo))reply
{
    NSLog(@"=======================userInfo : %@",userInfo);
    
    NSDictionary *result;
    
    if ([userInfo[@"REQUEST"] isEqualToString:@"GET_CURRENTDIC"]) {
        
        result = @{@"Percentage": self.mainViewController.percentageLabel.text,
                 @"Water": self.mainViewController.mlLabel.text};
        if (reply && result) {
            reply(result);
        }
        
//        result = [self getWaterFromHistory];
    }else if([userInfo[@"REQUEST"] isEqualToString:@"DRING_BUTTON_TOUCHED"]) {
        // TODO : 물마시기 버튼 터치
        [self.mainViewController drinkActionWithCompletion:^(NSDictionary *dic) {
            if (reply && dic) {
                reply(dic);
            }
        }];
//        - (void)drinkActionWithCompletion:(void (^)(NSDictionary *dic))completion
    }
}

- (NSDictionary *)getWaterFromHistory
{
    int percentage;
    int waterValue;
    
    NSMutableDictionary *historyDictionary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"historyDictionary"] mutableCopy];
    if (historyDictionary) {
        NSString *valueString = historyDictionary[[self todayString]];
        if (valueString) {
            valueString = [valueString stringByReplacingOccurrencesOfString:@"ml" withString:@""];
            valueString = [valueString stringByReplacingOccurrencesOfString:@"% /" withString:@"=="];
            NSArray *arr = [valueString componentsSeparatedByString:@"=="];
            percentage = [arr.firstObject intValue];
            waterValue = [arr.lastObject intValue];
        }else{
            percentage = 0;
            waterValue = 0;
        }
    }
    
    return @{@"Percentage": @(percentage),
             @"Water": @(waterValue)};
    
}

- (NSString *)todayString
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"MMM dd, yyyy";
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    formatter.locale = locale;
    NSString *dateKey = [formatter stringFromDate:[NSDate date]];
    return dateKey;
}

@end
