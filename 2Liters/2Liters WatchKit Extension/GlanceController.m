//
//  GlanceController.m
//  2Liters WatchKit Extension
//
//  Created by yoseop on 2015. 6. 29..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "GlanceController.h"
#import "MMWormhole.h"

@interface GlanceController()

@property (nonatomic, strong) MMWormhole *dataFromiPhone;

//@property (weak, nonatomic) IBOutlet WKInterfaceLabel *glanceGenre;
//@property (weak, nonatomic) IBOutlet WKInterfaceLabel *glanceName;
//@property (weak, nonatomic) IBOutlet WKInterfaceLabel *glanceMetaData;

@property (weak, nonatomic) IBOutlet WKInterfaceLabel *percentageLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *mlLabel;
@end


@implementation GlanceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    [self addWatchObserver];
}

- (void)willActivate {
    [super willActivate];
    [self getCurrentWater];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}


- (void)addWatchObserver
{
    self.dataFromiPhone = [[MMWormhole alloc] initWithApplicationGroupIdentifier:@"group.yoseop2da.2liters" optionalDirectory:@"2Liters"];
    [self.dataFromiPhone listenForMessageWithIdentifier:@"DataFromiPhone" listener:^(id messageObject) {
        if (messageObject) {
            [self.percentageLabel setText:messageObject[@"percentageString"]];
            [self.mlLabel setText:messageObject[@"mlString"]];
        }
    }];
}

- (void)getCurrentWater
{
    NSDictionary *dic = @{@"REQUEST": @"GET_CURRENTDIC"};
    [WKInterfaceController openParentApplication:dic reply:^(NSDictionary *replyInfo, NSError *error) {
            NSLog(@"replyInfo : %@",replyInfo);
            
            [self.percentageLabel setText:replyInfo[@"Percentage"]];
            [self.mlLabel setText:replyInfo[@"Water"]];
    }];
}
@end



