//
//  InterfaceController.m
//  2Liters WatchKit Extension
//
//  Created by yoseop on 2015. 6. 29..
//  Copyright (c) 2015년 yoseop. All rights reserved.
//

#import "InterfaceController.h"

#import "MMWormhole.h"

@interface InterfaceController()

@property (weak, nonatomic) IBOutlet WKInterfaceLabel *percentageLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *mlLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceGroup *loadingGroup;
@property (nonatomic, strong) MMWormhole *dataFromiPhone;

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    [self performSelector:@selector(hideLoadingImage) withObject:nil afterDelay:2.0f];
    [self addWatchObserver];
}

- (void)willActivate {
    [super willActivate];
    [self getCurrentWater];

}

- (void)didDeactivate {
    [super didDeactivate];
}

- (IBAction)drinkButtonTouched
{
    NSDictionary *dic = @{@"REQUEST": @"DRING_BUTTON_TOUCHED"};
    [WKInterfaceController openParentApplication:dic reply:^(NSDictionary *replyInfo, NSError *error) {
        if (error) {
            [self consoleLogWithError:error methodName:NSStringFromSelector(_cmd)];
        }else{
            NSLog(@"replyInfo : %@",replyInfo);
            [self.percentageLabel setText:replyInfo[@"Percentage"]];
            [self.mlLabel setText:replyInfo[@"Water"]];

        }
    }];
}

- (void)getCurrentWater
{
    NSDictionary *dic = @{@"REQUEST": @"GET_CURRENTDIC"};
    [WKInterfaceController openParentApplication:dic reply:^(NSDictionary *replyInfo, NSError *error) {
        if (error) {
            [self consoleLogWithError:error methodName:NSStringFromSelector(_cmd)];
        }else{
            NSLog(@"replyInfo : %@",replyInfo);

            [self.percentageLabel setText:replyInfo[@"Percentage"]];
            [self.mlLabel setText:replyInfo[@"Water"]];

        }
    }];
}

- (void)hideLoadingImage
{
    [self.loadingGroup setHidden:YES];
}

- (void)consoleLogWithError:(NSError *)error methodName:(NSString *)methodName
{
    NSLog(@"_________Error[%@][%@]:%@",NSStringFromClass([self class]),methodName,error.localizedDescription);
}

- (void)addWatchObserver
{
    self.dataFromiPhone = [[MMWormhole alloc] initWithApplicationGroupIdentifier:@"group.yoseop2da.2liters" optionalDirectory:@"2Liters"];
    [self.dataFromiPhone listenForMessageWithIdentifier:@"DataFromiPhone" listener:^(id messageObject) {
        if (messageObject) {
            [self.percentageLabel setText:messageObject[@"percentageString"]];
            [self.mlLabel setText:messageObject[@"mlString"]];
        }
    }];
}


@end



